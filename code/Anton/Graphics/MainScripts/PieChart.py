
# coding: utf-8

# In[3]:

import matplotlib.pyplot as plt
import numpy as np

N = 10
x = [i for i in range(0, N)]
z = np.random.random(N)
s = [str(i) for i in x]
    
fig = plt.figure()
plt.pie(z, labels=s, autopct='%i%%')
plt.title('PieChart')

plt.savefig('PieChat.png', fmt='png')
plt.show()


# In[ ]:



