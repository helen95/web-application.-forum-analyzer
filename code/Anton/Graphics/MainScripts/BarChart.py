
# coding: utf-8

# In[2]:

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm

N = 100
x = [i for i in range(0, N)]
z = np.random.random(N)


imin = 0
imax = 0
for i in range(0, len(z)):
    if (z[imin] > z[i]):
        imin = i
    if (z[imax] < z[i]):
        imax = i

fig = plt.figure()
plt.title('Bar')
plt.grid(True)
for i in range(0, len(x)):
    if (i == imin):
        plt.bar(x[i], z[i], color = 'black')
    elif (i == imax):
        plt.bar(x[i], z[i], color = 'red')
    else:
        plt.bar(x[i], z[i], color = 'blue')

plt.savefig('BarChat.png', fmt='png')

plt.show()


# In[ ]:




# In[ ]:



