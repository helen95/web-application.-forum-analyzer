
# coding: utf-8

# In[4]:

import matplotlib.pyplot as plt
import numpy as np

def f_plot(xlist, zlist, **kwargs):
    colors = kwargs.pop('colors', 'k')
    linewidth = kwargs.pop('linewidth', 1.)
    
    fig = plt.figure()
    
    i = 0
    for x, y, color in zip(xlist, zlist, colors):
        i += 1
        plt.plot(x, y, color = color, linewidth = linewidth, label = color)
    
    plt.grid(True)
    plt.legend()
    
    plt.savefig('GraphicChat.png', fmt='png')
    plt.show()

N = 30 #variance
C = 3 #count of lines
x = [np.arange(N) for i in range(0, C)]
z = [np.random.random(N) for i in range(0, C)] 
colors = ['red', 'blue', 'green']

f_plot(x, z, colors = colors, linewidth = 2.)


# In[ ]:




# In[ ]:



