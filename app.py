#! /usr/bin/env python
# -*- coding: utf-8 -*-
#encoding 'utf-8'
from flask import Flask, render_template, request, redirect, url_for, flash
#from flask_sqlalchemy import SQLAlchemy
from flask import jsonify
#from flaskext.mysql import MySQL
#from flask.ext.stormpath import StormpathError, StormpathManager, User, login_required, login_user, logout_user, user
from flask_wtf import Form
from flask_wtf import CSRFProtect as CsrfProtect
#from wtforms import SelectMultipleField, widgets

#download data
import pandas as pd
import nltk
import string
import pandas as pd
import numpy as np
import math
import datetime

from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize
from pandas import DataFrame
nltk.data.path.append('/home/administrator/nltk_data/')

full_text=[[],[],[],[],[],[],[],[]]
#если ошибка - удалить строку с номером ??? того. где пишет ошибку (не ясно почему)
for i in range(0,8):
    full_text[i]=pd.DataFrame
    full_text[i]=pd.read_csv('data/class_'+str(i)+'.csv',sep='|',encoding='utf-8',low_memory=False,error_bad_lines=False)
    
    del full_text[i]['Unnamed: 0']
    
    try:
        del full_text[i]['Unnamed: 0.1']
    except:
        pass
f_t=full_text[0].append([full_text[1],full_text[2],full_text[3],full_text[4],full_text[5],full_text[6],full_text[7]],ignore_index=True)
print(type(f_t['tag1'][0]))
full_text=[[],[],[],[],[],[],[],[]]
print('1')
"""#если ошибка - удалить строку с номером ??? того. где пишет ошибку (не ясно почему)
for i in range(0,8):
    full_text[i]=pd.DataFrame
    full_text[i]=pd.read_csv('data/b('+str(i)+')_norm.csv',sep='|',encoding='utf-8',low_memory=False,error_bad_lines=False)
    
    del full_text[i]['Unnamed: 0']
    
    try:
        del full_text[i]['Unnamed: 0.1']
    except:
        pass

f_t2=full_text[0].append([full_text[1],full_text[2],full_text[3],full_text[4],full_text[5],full_text[6],full_text[7]],ignore_index=True)
full_text=[[],[],[],[],[],[],[],[]]
print('2')

#если ошибка - удалить строку с номером ??? того. где пишет ошибку (не ясно почему)
for i in range(0,8):
    full_text[i]=pd.DataFrame
    full_text[i]=pd.read_csv('data/c('+str(i)+')_norm.csv',sep='|',encoding='utf-8',low_memory=False,error_bad_lines=False)
    
    del full_text[i]['Unnamed: 0']
    
    try:
        del full_text[i]['Unnamed: 0.1']
    except:
        pass

f_t3=full_text[0].append([full_text[1],full_text[2],full_text[3],full_text[4],full_text[5],full_text[6],full_text[7]],ignore_index=True)
full_text=[[],[],[],[],[],[],[],[]]
print('3')
#если ошибка - удалить строку с номером ??? того. где пишет ошибку (не ясно почему)
for i in range(0,8):
    full_text[i]=pd.DataFrame
    full_text[i]=pd.read_csv('data/d('+str(i)+')_norm.csv',sep='|',encoding='utf-8',low_memory=False,error_bad_lines=False)
    
    del full_text[i]['Unnamed: 0']
    
    try:
        del full_text[i]['Unnamed: 0.1']
    except:
        pass

f_t4=full_text[0].append([full_text[1],full_text[2],full_text[3],full_text[4],full_text[5],full_text[6],full_text[7]],ignore_index=True)
full_text=[[],[],[],[],[],[],[],[]]
print('4')
#если ошибка - удалить строку с номером ??? того. где пишет ошибку (не ясно почему)
for i in range(0,8):
    full_text[i]=pd.DataFrame
    full_text[i]=pd.read_csv('data/e('+str(i)+')_norm.csv',sep='|',encoding='utf-8',low_memory=False,error_bad_lines=False)
    
    del full_text[i]['Unnamed: 0']
    
    try:
        del full_text[i]['Unnamed: 0.1']
    except:
        pass

f_t5=full_text[0].append([full_text[1],full_text[2],full_text[3],full_text[4],full_text[5],full_text[6],full_text[7]],ignore_index=True)
full_text=[]
print('5')
"""

app = Flask(__name__, template_folder='templates')
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:asd123qwe@localhost/postgres'

app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'some_really_long_random_string_here'
app.config['STORMPATH_API_KEY_FILE'] = 'apiKey.properties'
app.config['STORMPATH_APPLICATION'] = 'flaskr'
 
# These settings disable the built-in login / registration / logout functionality
# Stormpath provides so we can make our own custom ones later!
app.config['STORMPATH_ENABLE_LOGIN'] = False
app.config['STORMPATH_ENABLE_REGISTRATION'] = False
app.config['STORMPATH_ENABLE_LOGOUT'] = False
app.config.from_object(__name__)

#app.secret_key = 'some_secret'
CsrfProtect(app)


#function set_main_param
def set_main_params():
    group=-1
    numer=-1
    string=[]
    for line in f_t.groupby(['tag1']):
        group=group+1
        th=(line[0].encode('utf-8'),len(line[1]))
        
        subths=[]
        for line2 in line[1].groupby(['tag2']):
            numer=numer+1
            subths.append(((line2[0].encode('utf-8')).replace('\n',''),str(len(line2[1])),'ch_'+str(numer)))
            #print('\t',len(line2[1]),line2[0]) 
            #unstring=unstring+"('"+str(line2[0])+"','"+str(len(line2[1]))+"') , "
        #unstring=unstring[:-3]
        count=len(line[1].groupby(['tag2']))
        #print(count)
        #print(subths)
        string.append(dict(th=th,count=count,subths=subths))
        #string=string+unstring+"]},"
        
    #string=string[:-1]
    #string=string.replace('\n','')

    return string


def get_tokens(text):
    if isinstance(text, str):
        stemmer = SnowballStemmer("russian", ignore_stopwords=True)
        words = word_tokenize(text)
        tokens = []
        for w in words:
            tokens.append(stemmer.stem(w))

        stop_words = set(stopwords.words('russian'))
        stop_words.update(list(string.punctuation))
        #stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', "-", "`", "\", '/', \
        #                    '=', '*', '^', '%', '+', '#', '<', '>']) # remove this if you need punctuation 

        list_of_words = []
        for doc in tokens:
            list_of_words += [i.lower() for i in wordpunct_tokenize(doc) if i.lower() not in stop_words]

        return list_of_words
    return ["NaN"]

def get_words_of(list1,list2):
    n=0
    for word in list1:
        if word in list2:
            n=n+1
    return n

def searching_by(text,check):
    #print(check)
    if '1' not in check:
        check2=''
        for el in check:
            check2=check2+'1'
        #print(check2)
        check=str(check2)
    zapros=get_tokens(text)
    arr=[]
    if len(zapros)>0:
        for i in range(0,len(zapros)):
            arr.append([])
        num=-1
        for groups in f_t.groupby(['tag1','tag2']):
            num=num+1

            if (check[num]=='1')or(int(check[num])==1):
                for line in groups[1].index:
                    lenword=[]
                    lenword.extend(f_t2.get_value(line,'text_short_norm')[1:-1].replace('"','').replace(' ','').replace("'",'').split(','))
                    lenword.extend(f_t2.get_value(line,'text_full_norm')[1:-1].replace('"','').replace(' ','').replace("'",'').split(','))
                    nums=get_words_of(zapros,lenword)
                    if(nums>0):
                        arr[len(zapros)-nums].append(line)
        arr2=[]
        for i in range(0,len(zapros)):
            arr2.extend(arr[i])
        arr=[]
    else:
        #print(check)
        arr2=[]
        num=-1
        for groups in f_t.groupby(['tag1','tag2']):
            num=num+1
            #print(groups[0])
            #print(num,check[num])
            if (check[num]=='1')or(int(check[num])==1):
                #print('Op')
                #print(groups[1].index)
                for line in groups[1].index:
                    arr2.append(line)
                    #print(line)
    #print(arr2)
    return arr2


def set_second_params(paramet):
    arr=paramet
    param = []
    for i  in arr:
        #print(i)
        if '|' not in f_t.get_value(i,'text_short'):
            try:
                #print('try..')
                count = int(f_t2.get_value(i, 'ans_count'))
                #print(count)
                q_short=f_t.get_value(i,'text_short')
                #print(q_short)
                q_full=f_t.get_value(i, 'text_full')
                #print(q_full)
                q_date = f_t5.get_value(i, 'date')
                #print(q_date)
                q_weigth = f_t4.get_value(i, 'weight')
                    
                param.append(dict(q_num=i, q_short=q_short, q_full = q_full, q_date=q_date, q_weigth=q_weigth, count=count))
            except:
                #print('end by error')
                pass
    return param


@app.route('/', methods=['GET', 'POST'])
def index2():
    count = 5
    new_param = set_main_params() 
    #print(new_param)
    #if request.form.get('csrf_token'):
    #    return get_data()
    if request.method == 'POST':
        value = request.form.getlist('csrf_token') 
        #print(value)
        return "OK"
    
    return render_template('page.html', count=count, param=new_param)

#SORT ['DATE'] #getting array, return array
def sorting_date(num):
    arr=set(num)
    arr2=[]
    for i in f_t5.select(lambda x: x in arr).sort_values('date', ascending=False).index:
        arr2.append(i)
    return arr2

#SORT ['POPULARITY'] #getting array, return array
def sorting_weigth(num):
    arr=set(num)
    arr2=[]
    for i in f_t4.select(lambda x: x in arr).sort_values('weight', ascending=False).index:
        arr2.append(i)
    return arr2

#SORT ['ANS_COUNT'] #getting array, return array
def sorting_ans_count(num):
    arr=set(num)
    arr2=[]
    for i in f_t2.select(lambda x: x in arr).index:
        try:
            f_t2.set_value(i,'ans_count',int(f_t2.get_value(i,'ans_count'))) #making them integer PRINUDITELNO
        except:
            f_t2.set_value(i,'ans_count',0)
    for i in f_t2.select(lambda x: x in arr).sort_values('ans_count', ascending = False).index:
        arr2.append(i)
    return arr2


#SORT ['QUE_LEN'] #getting array, return array
def sorting_que_len(num):
    arr=set(num)
    arr2=[]
    num=-1
    for i in arr:#f_t.select(lambda x: x in arr).index:
        num=num+1
        arr2.append((i,len(str(f_t.get_value(i,'text_short')))+len(str(f_t.get_value(i,'text_full')))))
    arr3=sorted(arr2, key=lambda item: -item[1])
    for i in range(0,len(arr)):
        arr2[i]=arr3[i][0]
    return arr2


#SORT ['BETWEEN_DATE'] #getting two dates, return dataframe
def sorting_between_date(dataframe, d1, d2): #d1 - first date, d2 - second date, - in format: ('%d.%m.%Y')
    dataframe.date = pd.to_datetime(dataframe.date,dayfirst=True)
    d1 = datetime.datetime.strptime(d1, '%d.%m.%Y')
    d2 = datetime.datetime.strptime(d2, '%d.%m.%Y')
    d1 = datetime.datetime.date(d1)
    d2 = datetime.datetime.date(d2)
    re_df = dataframe[dataframe.date.between(d1, d2)]
    return re_df.sort_values('date')
	
@app.route("/questions", methods=['GET', 'POST'])
def get_questions():
    print(request.args)
    value=request.args.get('search')
    value3=str(request.args.get('checkboxes'))
    print(value)

    new_param = set_second_params(searching_by(value,value3))


    if request.args.get('search_by') == 'sort1':
        
        value2 = request.args.get('search')
        arr=searching_by(value,value3)
        param = set_second_params(sorting_ans_count(arr))
        return render_template('questions.html', param=param, size=len(param))

    elif request.args.get('search_by') == 'sort2':
        value = request.args.get('search')
        arr=searching_by(value,value3)
        param = set_second_params(sorting_date(arr))
        return render_template('questions.html', param=param, size=len(param))

    elif request.args.get('search_by') == 'sort3':
        value = request.args.get('search')
        arr=searching_by(value,value3)
        param = set_second_params(sorting_que_len(arr))
        return render_template('questions.html', param=param, size=len(param))

    elif request.args.get('search_by') == 'sort4':
        value = request.args.get('search')
        arr=searching_by(value,value3)
        param = set_second_params(sorting_weigth(arr))
        return render_template('questions.html', param=param, size=len(param))

    return render_template('questions.html', param=new_param, size=len(new_param))


@app.route("/articles", methods=['GET'])
def get_articles():
    return render_template('articles.html')

@app.route("/state/<number>", methods=['GET'])
def get_state(number):
    return render_template('states/state_'+str(number)+'.htm')

def get_short_quest(num):
    return f_t.get_value(num,'text_short')

def get_full_quest(num):
    strr=f_t.get_value(num, 'text_full')
    if (strr!='0'):
        return strr
    return get_short_quest(num)

def get_full_date(num):
    return f_t5.get_value(num, 'date')

def get_anss(num):
    q_count = int(f_t2.get_value(num, 'ans_count'))
    if(q_count>17):
        q_count=17
    res = []
    for j in range(q_count):
        ans = f_t3.get_value(num, 'ans' + str(j + 1) + '_text')
        date = f_t3.get_value(num, 'ans' + str(j + 1) + '_date')
        res.append( (ans, date) )
    return res

@app.route("/answers", methods=['GET'])
def get_answers():

    num = int(request.args.get('num'))
    short_quest = get_short_quest(num)
    full_quest = get_full_quest(num)
    q_date = get_full_date(num)
    all_answers = get_anss(num)
    return render_template('answers.html', short_quest=short_quest, full_quest=full_quest, all_answers=all_answers, q_date=q_date)


if __name__ == "__main__":  
    #app.run(debug=False, host='0.0.0.0', port=8317)
    app.run(debug=True, port=8317)